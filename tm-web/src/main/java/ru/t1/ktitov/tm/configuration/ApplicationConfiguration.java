package ru.t1.ktitov.tm.configuration;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.ktitov.tm")
public class ApplicationConfiguration {
}
