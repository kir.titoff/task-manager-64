package ru.t1.ktitov.tm.service.model;

import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.service.model.IService;
import ru.t1.ktitov.tm.model.AbstractModel;

@Service
public abstract class AbstractService<M extends AbstractModel> implements IService<M> {
}
