package ru.t1.ktitov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task create(@Nullable String userId, @Nullable String name);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    List<Task> findAll(@Nullable String userId, @Nullable Sort sort);

}
