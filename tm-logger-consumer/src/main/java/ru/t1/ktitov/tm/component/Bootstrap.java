package ru.t1.ktitov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.service.ReceiverService;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private ReceiverService receiverService;

    public void run() {
        receiverService.receive();
    }

}
