package ru.t1.ktitov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.task.TaskBindToProjectRequest;
import ru.t1.ktitov.tm.event.ConsoleEvent;
import ru.t1.ktitov.tm.util.TerminalUtil;

@Component
public final class TaskBindToProjectListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-bind-to-project";

    @NotNull
    public static final String DESCRIPTION = "Bind task to project";

    @Override
    @EventListener(condition = "@taskBindToProjectListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.print("ENTER TASK ID: ");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        taskEndpoint.bindTaskToProject(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
