package ru.t1.ktitov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.data.DataJsonFasterXmlLoadRequest;
import ru.t1.ktitov.tm.event.ConsoleEvent;

@Component
public class DataJsonFasterXmlLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-json-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Load data from json file by fasterxml";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonFasterXmlLoadListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull DataJsonFasterXmlLoadRequest request = new DataJsonFasterXmlLoadRequest(getToken());
        domainEndpoint.loadDataJsonFasterXml(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
