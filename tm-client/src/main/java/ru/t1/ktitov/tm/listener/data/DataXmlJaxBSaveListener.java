package ru.t1.ktitov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.data.DataXmlJaxBSaveRequest;
import ru.t1.ktitov.tm.event.ConsoleEvent;

@Component
public class DataXmlJaxBSaveListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-xml-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Save data in xml file by jaxb";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlJaxBSaveListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull DataXmlJaxBSaveRequest request = new DataXmlJaxBSaveRequest(getToken());
        domainEndpoint.saveDataXmlJaxB(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
